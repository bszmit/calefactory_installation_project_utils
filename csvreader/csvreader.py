# Created by bszmit on 11/14/15
from typing import Tuple, List

__author__ = 'bszmit'


class CSVReader(object):
    """
    Opens and reads CSV file.
    """
    def __init__(self, filename: str):
        self.file = open(filename, 'r')
        self.keys = self._generate_dictionary_keys()

    def _generate_dictionary_keys(self) -> Tuple[str]:
        """
        Generates keys for dictionary from first line of CSV file.
        """
        line = self.file.readline()
        keys = tuple(x.strip() for x in line.split(','))
        return keys

    def _unpack_line(self, line) -> List[float]:
        """
        Extracts numerical values from given string line.

        :param line: string line
        :return: extracted numerical vales
        """
        float_vals = map(float, line.split(','))
        return float_vals

    def read_generator(self):
        """
        Reads data from file.

        :return: yields dictionary of keys and corresponding values.
        """
        for line in self.file:
            vals = self._unpack_line(line)
            yield dict(zip(self.keys, vals))


if __name__ == '__main__':
    csvReader = CSVReader('../../calefactory_installation_project/radiator_building_openloop_tests.txt')
    for result in csvReader.read_generator():
        print(result)

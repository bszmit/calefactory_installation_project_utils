# Created by bszmit on 11/14/15

from collections import deque
from copy import copy

__author__ = 'bszmit'


def to_list(f):
    """
    Decorator. Casts decorated function output to list.

    :param f: decorated function
    :return:
    """
    def wrapper(*args):
        return list(f(*args))

    return wrapper


class DataStorage(object):
    """
    Stores data.
    """
    def __init__(self, maxlen):
        """
        :param maxlen: length of data collections.
        :return:
        """
        tmp_deque = deque([0] * maxlen, maxlen=maxlen)
        self._To = copy(tmp_deque)
        self._Tzco = copy(tmp_deque)
        self._Day = copy(tmp_deque)
        self._Hour = copy(tmp_deque)
        self._Min = copy(tmp_deque)
        self._Fcob = copy(tmp_deque)
        self._Tpco = copy(tmp_deque)
        self._Tr = copy(tmp_deque)

    def update(self, To, Tzco, Day, Hour, Min, Fcob, Tpco, Tr):
        """
        Stores given values.
        """
        Day /= 100
        Hour /= 100
        Min /= 100
        self._To.append(To)
        self._Tzco.append(Tzco)
        self._Day.append(Day)
        self._Hour.append(Hour)
        self._Min.append(Min)
        self._Fcob.append(Fcob)
        self._Tpco.append(Tpco)
        self._Tr.append(Tr)

    @property
    @to_list
    def To(self):
        return self._To

    @property
    @to_list
    def Tzco(self):
        return self._Tzco

    @property
    @to_list
    def Day(self):
        return self._Day

    @property
    @to_list
    def Hour(self):
        return self._Hour

    @property
    @to_list
    def Min(self):
        return self._Min

    @property
    @to_list
    def Fcob(self):
        return self._Fcob

    @property
    @to_list
    def Tpco(self):
        return self._Tpco

    @property
    @to_list
    def Tr(self):
        return self._Tr

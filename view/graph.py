# Created by bszmit on 11/14/15

import matplotlib.pyplot as plt

__author__ = 'bszmit'


class Graph(object):
    """
    Creates real-time plot.
    """

    def __init__(self, width, nr_of_lines):
        """
        :param width: width of time window. Length of data.
        :param nr_of_lines: number of independent graph lines
        :return:
        """
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)

        # collect lines
        self.lines = self._collect_lines(width, nr_of_lines)

        # set y axis range
        self.set_ylim([0, 300])

        # show figure
        self.fig.canvas.draw()
        plt.show(block=False)

    def _collect_lines(self, width, nr_of_lines):
        """
        Collects lines.

        :param nr_of_lines: quantity of lines to be collected
        :return: list of lines
        """
        tmp = list(range(width))
        lines = []
        for _ in range(nr_of_lines):
            tmp_line, = self.ax.plot(tmp, tmp)
            lines.append(tmp_line)
        return lines

    def update_plot(self, time, *args):
        """
        Redraws plot with given data.

        :param time: time data
        :param args: collections of data to be plotted
        :return:
        """
        self.ax.set_xticklabels(time)
        for line, data in zip(self.lines, args):
            line.set_ydata(data)
        self.fig.canvas.draw()

    def set_ylim(self, vec):
        """
        Sets y axis range
        Example: set_ylim([0, 300])

        :param vec: lower and upper bound for y axis. Ex: [0, 300]
        :return:
        """
        self.ax.set_ylim(vec)

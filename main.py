# Created by bszmit on 11/13/15

from csvreader.csvreader import CSVReader
from datastorage.datastorage import DataStorage

from view.graph import Graph

__author__ = 'bszmit'

files_prefix_path = '../../calefactory_installation_project/data_files/'
graph = Graph(20, 1)
graph.set_ylim([0, 1.5])
dataStorage = DataStorage(20)
# csvReader = CSVReader(files_prefix_path + 'radiator_building_openloop_tests.txt')
# csvReader = CSVReader(files_prefix_path + 'PID_tests.txt')
csvReader = CSVReader(files_prefix_path + 'myfifo')

for resultDict in csvReader.read_generator():
    dataStorage.update(**resultDict)
    graph.update_plot(dataStorage.Min, dataStorage.To)
    # graph.update_plot(dataStorage.Min, dataStorage.Tpco)

exit(0)


